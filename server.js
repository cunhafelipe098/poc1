'use strict'

import koa from 'koa'
import defineRoutes from './src/config/routes'

const app = new koa()

defineRoutes(app)

module.exports =  app