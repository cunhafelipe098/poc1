// Transpile all code following this line with babel and use '@babel/preset-env' (aka ES6) preset.
require("@babel/register")({
    presets: ["@babel/preset-env"]
});
// Import the rest of our application.
const app = require('./server.js')

const config = require('./src/config')

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/BDpoc1',  {
                                                useUnifiedTopology: true,
                                                useNewUrlParser: true
                                                });
mongoose.set ( 'useCreateIndex' , true )
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

if(!module.parent) {
    app.listen(config.port, config.ip, () => console.log(`server listening on ${config.ip}:${config.port}`))
}