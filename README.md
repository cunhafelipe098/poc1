# POC1
# financial-management

## Objective
This project serves as a parameter for performance evaluation during the internship period, with the objective of controlling users' finances, in order to record their income and expenses, addressing the principles of: JavaScript, Node, MongoDB and REST.

## Prerequisites
In order to run this project you must have the following installed: 
- NodeJS with npm
- MongoDB 

## Installing the Project
To download and install the project, execute the following commands:
```
git clone https://gitlab.com/cunhafelipe098/poc1.git
cd poc1
npm install
```

## Running the Project
The run the project execute this command: 
```
npm start
```
- The routes are specified in the files into the routes folder
- Tests were done by postman 
