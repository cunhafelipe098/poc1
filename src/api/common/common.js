
import mongoose from 'mongoose'
        
const recordSchema = new mongoose.Schema({
    
    value: {
        type: Number,
        require: true
    },
    typeRecord: {
        type: String,
        require: true
    },
    date: {
        type: String, 
        default: Date.now
    },
    description: {
        type: String
    }
}) 

const userSchema = new mongoose.Schema({

    fistName: { type: String 
    },
    email: {

        type: String,
        required: true,
        index: true,
        unique: true,
    },
    records: [recordSchema]
    ,
    balance: {
        type: Number,
        default: 0.0
    }

})

const userModel = mongoose.model('user', userSchema, "users")
export default userModel