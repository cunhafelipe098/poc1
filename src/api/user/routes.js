'use strict'

import controller from './controller'
import router from 'koa-router'

const user = new  router()

user.put('/', controller.updateUser)
user.get('/', controller.allUsers)
user.get('/id', controller.findByUser)
user.post('/', controller.registerUser)
user.delete('/', controller.deleteUser)

module.exports = user