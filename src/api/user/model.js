'use strict'

const model = {
        
    User(data) {

        let {fistName, email } = data
        const user = {fistName, email}
    
        return !this.filter(email) || !fistName ? {err: 'invalid data'} : user  
    },
    
    validate(data) {

        let {fistName, email} = data
        
        let user = {};
        let err = {};

        (fistName != undefined) ? user.fistName = fistName : user 

        if(email != undefined)
            !this.filter(email) ? user.email = 'invalid email' : user.email = email
        
        return user
    },

    filter(email) {
    
        let filter = /^([\w-]+(?:.[\w-]+))@((?:[\w-]+.)\w[\w-]{0,66}).([a-z]{2,6}(?:.[a-z]{2})?)$/i;
        return filter.test(email)
    }
}

export default model
