'use strict'
import mongoose from 'mongoose'
import userModel from '../common'

const repository = {
    
    async allUsers () {
        
        try{
            return await userModel.find({}).lean()
        } catch (err){
            return {err: err.message}
        }
    },

    async findByUser (dataUniqueUser) {

        try{
            return await userModel.find(dataUniqueUser).lean()
        } catch (err){
            return {err: err.message}
        }
    },

    async insertUser(dataUser) {
        
        const newUser = new userModel(dataUser)
        
        try{
            return await newUser.save()
        } catch (err){
            return {err: err.message}
        }    
    },

    async putUser(dataUser) {
        
        const {id, ...update} = dataUser
        
        try{           
            return await userModel.findOneAndUpdate({_id: id}, update, { new: true })
        }catch(err) {
            return {err: err.message}
        }
    },

    async deleteUser(_id) {

        try{
            return await userModel.findByIdAndRemove(_id).exec()
        }catch{
            return {err: err.message}
        }
    }
}

export default repository