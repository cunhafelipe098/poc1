'use strict'
import repository from './repository'
import model from './model'

import { get } from 'lodash'

const service = {
        
    async allUsers () {

        return await repository.allUsers()
    },

    async findByUser(data) {

        const dataUnique = { email: get(data, 'email'),_id : get(data, 'id') }
       
        if(!dataUnique.email & !dataUnique._id)
            return { err: 'Sorry, it was not possible to find user' }
            
        if(dataUnique.email != undefined & dataUnique._id != undefined)
            return await repository.findByUser(dataUnique)

        return  dataUnique._id ? await repository.findByUser({_id : dataUnique._id}) : repository.findByUser({email : dataUnique.email}) 
    },

    async insertUser(dataUser) {

        const user = model.User(dataUser)
        return await user.err == 'invalid data' ? {err: 'invalid data'} : repository.insertUser(user)
        
    },

    async deleteUser(data) {

        const {id} = data
        
        if(!id)
            return { err: 'Sorry, required id ' }
        return await repository.deleteUser({_id : id})
    },

    async updadeDataUser(dataUser){

        let user = model.validate(dataUser)

        if(Object.keys(user).length === 0)
            return {err: 'There is no data to be updated'}   
        
        if( user.email ==  'invalid email') 
            return {err: 'invalid email'}
    
        user.id = dataUser.id
        return !user.id ?  {err: 'requeried id'} : await repository.putUser(user)
    }
}
export default service
