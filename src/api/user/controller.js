'use strict'
import service from './service'


const controller = {
        
    async allUsers (ctx) {

        ctx.response.body = await service.allUsers()
        
    },

    async findByUser(ctx) {

        ctx.response.body = await service.findByUser(ctx.request.body)
    },

    async registerUser(ctx) {

       ctx.response.body = await service.insertUser(ctx.request.body)
    },

    async deleteUser(ctx) {

        ctx.response.body = await service.deleteUser(ctx.request.body)
    },

    async updateUser(ctx) {

        ctx.response.body = await service.updadeDataUser(ctx.request.body)
    }
}

export default controller