import repository from './repository'
import model from './model'
import { get } from 'lodash'

const service = {
        
    async allRecordsOfUser(data) {

        const idUser = { _id: get(data, 'idUser')}

        if(!idUser._id)
            return { error: 'Sorry, required idUser' }
        
       return await repository.allRecordsOfUser(idUser)
    },

    async findByRecord(data) {

        const dataUnique = { idUser: get(data, 'idUser'), idRecord : get(data, 'idRecord') }

        if(!dataUnique.idUser || !dataUnique.idRecord)
            return { error: 'Sorry, it was not possible to find Record' }

        return await repository.findRecordsByid(dataUnique)
        
    },
    
    async insertRecords(dataRecord) {

        const dataUnique = { _id: get(dataRecord, 'idUser') }
        
        if(!dataUnique._id)
            return { error: 'Sorry, required idUser' }
        
        const user = model.Records(dataRecord)

        return (user.err == 'invalid data') ? {err: 'invalid data'} : await repository.insertRecords(dataUnique,user)
    },

    async deleteRecord(data) {
        
        const dataUnique = { idRecord: get(data, 'idRecord'), idUser: get(data, 'idUser') }
        
        if(!dataUnique.idRecord || !dataUnique.idUser)
            return { error: 'Sorry, required idRecord and idUser' }

        return await repository.deleteRecord(dataUnique)
    },

    async updadeDataRecord(data){

        const dataUnique = { idRecord: get(data, 'idRecord'), idUser: get(data, 'idUser') }

        if(!dataUnique.idRecord || !dataUnique.idUser)
            return { error: 'Sorry, required idRecord and idUser' }

        if(Object.keys(data).length == 2)
            return { error: 'Sorry, there is no data to be updated' }

        const user = model.validate(data)

        return (user.err == 'invalid data') ? {err: 'invalid data'} : await repository.putRecord(dataUnique, user) 
    }
}

export default service