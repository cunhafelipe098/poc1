'use strict'

import controller from './controller'
import router from 'koa-router'

const record = new router()

record.put('/', controller.updateRecord)
record.get('/', controller.allRecordsOfUser)
record.get('/id', controller.findByRecord)
record.post('/', controller.registerRecords)
record.delete('/', controller.deleteRecord)

module.exports = record
