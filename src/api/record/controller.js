import service from './service'

const controller = {
        
    async allRecordsOfUser(ctx) {

        ctx.response.body = await service.allRecordsOfUser(ctx.request.body)
    },

    async findByRecord(ctx) {

        ctx.response.body = await service.findByRecord(ctx.request.body)
    },

    async registerRecords(ctx) {

        ctx.response.body = await service.insertRecords(ctx.request.body)      
    },

    async deleteRecord(ctx) {
        
        ctx.response.body = await service.deleteRecord(ctx.request.body)
    },

    async updateRecord(ctx) {
    
        ctx.response.body = await service.updadeDataRecord(ctx.request.body)
    },
}

export default controller