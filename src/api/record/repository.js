'use strict'

import userModel from '../common'   
import { abs } from 'mathjs'
const repository = {

    find(arrayRecords, idRecord) {
        
        let record = {}
        arrayRecords.forEach((el) => {

            (el._id == idRecord) ? record = el : record
        })       
        return record
    },
    
    async findRecordsByid(dataUnique) {
        
        try{
            let user = await userModel.findOne({_id: dataUnique.idUser})

            return this.find(user.records ,dataUnique.idRecord)

        } catch (err){
            return {err: err.message}
        }
    },

    async allRecordsOfUser(idUser) {
        try{

            let user = await userModel.findOne({_id: idUser})

            return user.records

        } catch (err){
            return {err: err.message}
        }
    },

    async deleteRecord(dataUnique) {
        
        try{

            let user = await userModel.findOne({_id: dataUnique.idUser})

            const record = this.find(user.records ,dataUnique.idRecord)
            
            if (Object.keys(record).length == 0) 
                return {err: 'idRecord not found '}
            
            const indexRecord = user.records.indexOf(record);
            
            (user.records[indexRecord].typeRecord == 'expense') ?  
            user = this.UpdateBalanceUser(user, { typeRecord: 'income', value: user.records[indexRecord].value}):
            user = this.UpdateBalanceUser(user, { typeRecord: 'expense', value: user.records[indexRecord].value})
            
            user.records[indexRecord].remove()
            return await user.save()

        } catch (err){
            return {err: err.message}
        }
    },

    async putRecord(dataUnique, updateRecord) {
        
        try{
            let user = await userModel.findOne({_id: dataUnique.idUser})

            const record = this.find(user.records ,dataUnique.idRecord)
           
            if (Object.keys(record).length == 0) 
                return {err: 'idRecords not found '}

            const indexRecord = user.records.indexOf(record)

            updateRecord.date ? user.records[indexRecord].date = updateRecord.date : user
            updateRecord.description ? user.records[indexRecord].description = updateRecord.description : user
            updateRecord.value ?  user = this.putRecordUpdateBalanceUser(user, updateRecord.value, indexRecord) : user
            updateRecord.typeRecord ? user.records[indexRecord].typeRecord = updateRecord.typeRecord : user
            
            return await user.save()
        } catch (err){
            return {err: err.message}
        }
    },
    
    async insertRecords(dataUnique, record) {

        try{
            let user = await userModel.findOne(dataUnique)
            
            user = this.UpdateBalanceUser(user, record)
            
            user.records.push(record)
            return await user.save()
        } catch (err){
            return {err: err.message}
        }
    },

    putRecordUpdateBalanceUser(user, value, indexRecord) {
    
        const dif = user.records[indexRecord].value - value;

        if((parseInt(dif) > 0) & (user.records[indexRecord].typeRecord == 'expense')) 
            user = this.insertRecordsUpdateBalanceUser(user, {typeRecord: 'income', 'value': abs(dif)})
        else if((parseInt(dif) < 0) & (user.records[indexRecord].typeRecord == 'expense'))
            user = this.insertRecordsUpdateBalanceUser(user, {typeRecord: 'expense', 'value': abs(dif)}); 
        
        if((parseInt(dif) > 0) & (user.records[indexRecord].typeRecord == 'income'))
            user = this.insertRecordsUpdateBalanceUser(user, {typeRecord: 'expense', value: abs(dif)})
        else if((parseInt(dif) < 0) & (user.records[indexRecord].typeRecord == 'income')) 
            user = this.insertRecordsUpdateBalanceUser(user, {typeRecord: 'income', value: abs(dif)});

        user.records[indexRecord].value = value
        
        return user
    },
    
    UpdateBalanceUser(user,record) {
        
        record.typeRecord == 'expense' ? user.balance -= parseInt(record.value) : user.balance += parseInt(record.value)   
        return user
    },
}
export default repository