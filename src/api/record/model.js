'use strict'

const model = {
        
    Records(data) {

        let { value, typeRecord, date, description } = data
        
        let user = description ? { value, typeRecord, date, description } : { value, typeRecord, date }
        
        return !isNaN(value) & this.istypeRecord(typeRecord) & this.isdate(date) ? user : {err: 'invalid data'}   
    },

    istypeRecord(typeRecord) {

        return typeRecord == 'income' || typeRecord == 'expense' ? true : false 
    },

    isdate(date) {

        let patternData = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
        return patternData.test(date) ? true : false

    },

    validate(data) {

        let { value, typeRecord, date, description } = data
        
        let user = {};

        (!isNaN(value)) ? user.value = value : user 
        this.isdate(date) ? user.date = date : user
        description ? user.description = description : user
        this.istypeRecord(typeRecord) ? user.typeRecord = typeRecord : user
       
        return (Object.keys(user).length === 0) ? {err: 'invalid data'} :  user
    }
}

export default model