'use strict'

import 'babel-polyfill'
import mount from 'koa-mount'
import parser from 'koa-bodyparser'
import {user, record} from '../api'

export default function defineRoutes (app) {
    
   app
   .use(parser())
   .use(mount('/user', user.routes()))
   .use(mount('/record', record.routes()))
 
}