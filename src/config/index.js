

const config = {

    ip : process.env.IP || '127.0.0.1',
    port: process.env.PORT || 9000
}

module.exports = config